﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrandChaos
{
    public class ListWeapon
    {
        public static void LstWeapon(string[] args)
        {
            List<Weapon> Weapons = new List<Weapon>();
            Weapons.Add(new Weapon());
            Weapons[0].Name = "Dual Sword";
            Weapons[0].Damage = 5000;
            Weapons[0].ID = 1;

            Weapons.Add(new Weapon());
            Weapons[1].Name = "Longbow";
            Weapons[1].Damage = 3000;
            Weapons[1].ID = 2;

            Weapons.Add(new Weapon());
            Weapons[2].Name = "Spear";
            Weapons[2].Damage = 2000;
            Weapons[2].ID = 3;

            foreach (Weapon Choice in Weapons)
            {
                Console.WriteLine(Choice.Name);
            }
        }
    }
}