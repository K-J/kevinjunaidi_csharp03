﻿using System;
using System.Collections.Generic;

namespace GrandChaos
{
    class Program
    {
        static void Main(string[] args)
        {
            Player user = new Player();
            user.Weapon = new Weapon();

            Console.Write("Enter Your ID below : ");
            user.Name = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine("Welcome to Grand Chaos" + " " + user.Name);
            Console.WriteLine();

            List<Weapon> Weapons = new List<Weapon>();

            Weapons.Add(new Weapon());
            Weapons[0].Name = "Dual Sword";
            Weapons[0].Damage = 5000;
            Weapons[0].ID = 1;

            Weapons.Add(new Weapon());
            Weapons[1].Name = "Longbow";
            Weapons[1].Damage = 3000;
            Weapons[1].ID = 2;

            Weapons.Add(new Weapon());
            Weapons[2].Name = "Spear";
            Weapons[2].Damage = 2000;
            Weapons[2].ID = 3;
            {
                int index = 1;
                foreach (Weapon choice in Weapons)
                {
                    Console.WriteLine(index + "." + choice.Name + "(" + choice.Damage + ")");
                    index++;
                }

            selectweapon:
                Console.WriteLine();
                Console.WriteLine("Choose Your Weapon (by numbers) : ");
                int select;
                bool decided = int.TryParse(Console.ReadLine(), out select);
                if (decided == false)
                {
                    Console.WriteLine("please insert number!");
                    goto selectweapon;
                }

                int monsterHP = 10000;
                int hitbase = 0;
                foreach (Weapon choice in Weapons)
                {
                    if (select == choice.ID)
                    {
                        Console.WriteLine();
                        Console.WriteLine(choice.Name + "(" + choice.Damage + ")" + " " + "equipped");
                        Console.WriteLine("Monster with 10000 HP Appear, Defeat It!");
                        Console.WriteLine();

                        while (monsterHP > 0)
                        {
                            monsterHP -= choice.Damage;
                            hitbase++;
                            Console.WriteLine("Monster HP =" + " " + monsterHP);
                        }
                        Console.WriteLine(hitbase + " " + "turn");
                        Console.WriteLine();
                        Console.WriteLine(user.Name + " " + "had defeated the enemy in" + " " + hitbase + " " + "turn"); ;
                    }

                }
                    if (monsterHP > 0)
                    {
                        Console.WriteLine("The Number Doesn't Exist");
                        goto selectweapon;
                    }

                }
            }
        }
    }


    




