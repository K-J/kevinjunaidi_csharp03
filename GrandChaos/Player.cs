﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrandChaos
{
    public class Player
    {
        public string Name { get; set; }
        public Weapon Weapon { get; set; }
    }
}
