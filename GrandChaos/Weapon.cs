﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrandChaos
{
    public class Weapon
    {
        public string Name { get; set; }
        public int Damage { get; set; }
        public int ID { get; set; }
    }
}
